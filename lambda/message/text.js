const SUCCESS_SPEECH ='<say-as interpret-as="interjection">súper</say-as>';


module.exports = {
    es:{
        translation: {
            WELCOME_MSG:'Bienvenido a su juego "hagamos un cuento", este es el menú de juego, en el, podras elegir el numero de jugadores o escuchar las reglas, este es un juego de memoria para jugar de 2 a 4 personas, espero te diviertas con nosotros.',
            RULES1_MSG: 'Gracias por querer jugar con nosotros; el juego consiste en crear una historia pero palabra por palabra; para',
            RULES2_MSG: 'lograr el objetivo, cada jugador tendrá un turno en el cual deberá repetir las palabras anteriormente mencionadas y en orden; pero al final deberá agregar una más a la historia, lo',
            RULES3_MSG: 'explicaremos con un ejemplo: el dia de hoy Luis, Andrea y David van a jugar con nosotros; inicia Luis la historia diciendo la palabra "habia", después',
            RULES4_MSG: 'es el turno de Andrea, quien repite la palabra de Luis y agrega una nueva, entonces Andrea dice "Habia una"; por último es el turno de David quien repite la historia y agrega una nueva palabra, por lo tanto el dice, "habia una vez"; ahora',
            RULES5_MSG: 'Luis debe repetir la frase y agregar una palabra nueva, de esta manera se va armando una historia hasta que uno de los jugadores se equivoque, en este momento el juego terminará, ',
            RULES6_MSG: '¿fácil verdad? Después de esta explicación vamos a volver al menú y me dirás cuantas personas van a jugar y los nombres de los jugadores; una vez esto este listo podremos iniciar el juego, mucha suerte y creemos el mejor cuento.',
            PLAY_MSG: 'hora de jugar, el día de hoy tendremos en partida a: ',
            PLAYERS_MSG:'Excelente! vamos a jugar %s personas. Desde este momento puedes registrar a los jugadores diciendo "soy, seguido de tu nombre" ó me llamo y dices tu nombre; recuerda que si decides cambiar el número de jugadores debes volver a registrarlos a todos.',
            NO_PLAY_MSG: 'Todavia no has dicho cuantos jugadores somos, Puedes decirme el numero de jugadores para esta partida o volver a escuchar las reglas',
            REPROMPT_MSG: 'Puedes decirme el numero de jugadores para esta partida o volver a escuchar las reglas',
            REGISTER_MSG: SUCCESS_SPEECH + '; Bienvenido al juego: %s, eres el jugador número %s',
            COMPLETE_MSG:'Todos los jugadores ya han sido registrados',
            NEXT_MSG:'ya estamos listos para jugar, solo pideme que inicie el juego y que gane el mejor'
        }
    }
}
