/* *
 * This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
 * Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
 * session persistence, api calls, and more.
 * */
const Alexa = require('ask-sdk-core');
const inter = require('./interceptors.js');
//VARIABLES GLOBALES

var players = 0;
var state = 'START';
var register = false;
var nom_players =[];
var reg_play =0;


const LaunchRequestHandler = {
    canHandle(handlerInput) {
        
        return Alexa.isNewSession(handlerInput.requestEnvelope) 
      || Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
        const RequestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const speakOutput = RequestAttributes.t('WELCOME_MSG') ;
        players=0;
        nom_players =[];
        reg_play =0;
        register = false;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const IniciarJuegoIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'IniciarJuegoIntent';
    },
    handle(handlerInput) {
        const RequestAttributes = handlerInput.attributesManager.getRequestAttributes();
        var speakOutput = RequestAttributes.t('NO_PLAY_MSG') ;
        if (state === 'PLAY'){
            speakOutput = RequestAttributes.t('PLAY_MSG');
            for (var i=0;i<nom_players.length;i++){
                let a = i+1;
                speakOutput=speakOutput+"jugador "+ a +" " +nom_players[i]+"; ";
            }
            state = 'GAMING';
        }
        const speakReprompt = RequestAttributes.t('REPROMPT_MSG') ;
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakReprompt)
            .getResponse();
    }
};

const rulesIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'rulesIntent';
    },
    handle(handlerInput) {
        const RequestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const speakOutput = RequestAttributes.t('RULES1_MSG') + RequestAttributes.t('RULES2_MSG') + RequestAttributes.t('RULES3_MSG') + RequestAttributes.t('RULES4_MSG') + RequestAttributes.t('RULES5_MSG') + RequestAttributes.t('RULES6_MSG');
        const replay = RequestAttributes.t ('WELCOME_MSG');
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(replay)
            .getResponse();
    }
};
const NumberOfPlayersIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NumberOfPlayersIntent';
    },
    handle(handlerInput) {
        const RequestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;
        if(players!==0){
            for(var i=0;i<nom_players.length;i++){
                console.log(nom_players[i]);
                nom_players[i]=null;
            }
            reg_play=0;
        }
        players = intent.slots.jugadores.value;
        const speakOutput = RequestAttributes.t('PLAYERS_MSG', players);
        register = true;
        const replay = RequestAttributes.t ('WELCOME_MSG');
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(replay)
            .getResponse();
    }
};

const registerIntentHandler = {
    canHandle(handlerInput) {
        
      
           return register && Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'registerIntent';
       
        
    },
    handle(handlerInput) {
        const RequestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;
         var speakOutput = RequestAttributes.t('COMPLETE_MSG');
        if(reg_play==players){
            state="PLAY";
            register = false;
        }
        if(reg_play<players){
        var player = intent.slots.name.value;
        nom_players.push(player);
        speakOutput = RequestAttributes.t('REGISTER_MSG', player,reg_play+1);
        reg_play=reg_play+1;
            if(reg_play==players){
                register = false;
                state="PLAY";
             }
        }
        
        const replay = RequestAttributes.t ('NEXT_MSG');
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(replay)
            .getResponse();
    }
};





const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'You can say hello to me! How can I help?';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'Goodbye!';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};
/* *
 * FallbackIntent triggers when a customer says something that doesn’t map to any intents in your skill
 * It must also be defined in the language model (if the locale supports it)
 * This handler can be safely added but will be ingnored in locales that do not support it yet 
 * */
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const speakOutput = 'Sorry, I don\'t know about that. Please try again.';

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};
/* *
 * SessionEndedRequest notifies that a session was ended. This handler will be triggered when a currently open 
 * session is closed for one of the following reasons: 1) The user says "exit" or "quit". 2) The user does not 
 * respond or says something that does not match an intent defined in your voice model. 3) An error occurs 
 * */
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`~~~~ Session ended: ${JSON.stringify(handlerInput.requestEnvelope)}`);
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse(); // notice we send an empty response
    }
};
/* *
 * The intent reflector is used for interaction model testing and debugging.
 * It will simply repeat the intent the user said. You can create custom handlers for your intents 
 * by defining them above, then also adding them to the request handler chain below 
 * */
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
       // const speakOutput = `You just triggered ${intentName}`;
       
       const speakOutput = `No puedes realizar esa acción en este momento.`;
        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};
/**
 * Generic error handling to capture any syntax or routing errors. If you receive an error
 * stating the request handler chain is not found, you have not implemented a handler for
 * the intent being invoked or included it in the skill builder below 
 * */
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const speakOutput = 'Sorry, I had trouble doing what you asked. Please try again.';
        console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
}; 

/*ZONA DONDE SE HALOJAN LOS INTERCEPTORS*/




/**
 * This handler acts as the entry point for your skill, routing all request and response
 * payloads to the handlers above. Make sure any new handlers or interceptors you've
 * defined are included below. The order matters - they're processed top to bottom 
 * */
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        IniciarJuegoIntentHandler,
        rulesIntentHandler,
        NumberOfPlayersIntentHandler,
        registerIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        FallbackIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler)
    .addErrorHandlers(
        ErrorHandler)
    .withCustomUserAgent('sample/hello-world/v1.2')
    .addRequestInterceptors(
        inter.LocalizationInterceptors
        )
    .lambda();